/*
 ** TailwindCSS Configuration File
 **
 ** Docs: https://tailwindcss.com/docs/configuration
 ** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */
module.exports = {
  theme: {
    extend: {
      fontFamily: {
        'serif': ['Merriweather', 'Georgia', 'Cambria', 'serif'],
      },
      colors: {
        blue: {
          '200': '#E9F9FF',
          '300': '#D2F2FF',
          '700': '#29648A',
          '900': '#022D41',
        },
        'blue-green': '#4e92ab',
        pink: {
          '500': '#E6007E',
        },
        red: {
          '500': '#E63946',
        },
        gray: {
          '300': '#9aa4aa',
          '500': '#405562',
          '700': '#1b2329',
        }
      }
    }
  },
  variants: {
    textColor: ['responsive', 'hover', 'focus', 'group-hover'],
    opacity: ['responsive', 'hover', 'focus', 'group-hover'],
    padding: ['responsive', 'group-hover'],
    visibility: ['responsive', 'group-hover'],
  },
  plugins: [require('tailwind-bootstrap-grid')()],
}
