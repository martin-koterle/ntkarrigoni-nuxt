import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const page = path => () => import(`~/pages/${path}`).then(m => m.default || m)

const routes = [
  { path: '/', name: 'index', component: page('index.vue') },

  { path: '/novice', name: 'articles.index', component: page('articles/index.vue') },
  { path: '/novice/:slug', name: 'articles.show', component: page('articles/show.vue') },

  { path: '*', name: 'page', component: page('page.vue') },

]

export function createRouter () {
  return new Router({
    routes,
    mode: 'history'
  })
}
