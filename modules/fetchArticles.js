import fse from 'fs-extra'
import axios from 'axios'

export default function fetchData(moduleOptions) {
  this.nuxt.hook('build:before', async ({
    app
  }) => {

    const writeData = (path, data) => {
      return new Promise((resolve, reject) => {
        try {
          fse.writeFile(path, JSON.stringify(data), () => {
            resolve(`${path} Write Successful`)
          })
        } catch (e) {
          console.error(`${path} Write Failed. ${e}`)
          reject(`${path} Write Failed. ${e}`)
        }
      })
    }

    axios.get('http://arrigoni-twill.local/api/articles')
      .then(function (response) {
        // handle success
        writeData(`./static/data/articles.json`, response.data)
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      })

  })

}

// api/navigation - list of all navigateable pages
// api/pages - list of all published pages
// api/pages/{id} - single page
// loops through and fore each creates a json
